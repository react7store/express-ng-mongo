const express = require('express');
const router = express.Router();
const playerList = require('./playerlist');

router.get('/playerlist', (req, res) => {
    res.send(playerList)
});

module.exports = router;