const express = require('express')
  , passport = require('passport')
  , util = require('util')
  , BitbucketStrategy = require('passport-bitbucket-oauth2').Strategy;

const BITBUCKET_CLIENT_ID = "mywin28"
const BITBUCKET_CLIENT_SECRET = "ZD6U8XX8vxNb47JM5Mpcbm648KSZCA8T";



passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(obj, done) {
  done(null, obj);
});


passport.use(new BitbucketStrategy({
    clientID: BITBUCKET_CLIENT_ID,
    clientSecret: BITBUCKET_CLIENT_SECRET,
    callbackURL: "http://localhost:3000/auth/bitbucket/callback"
  },
  function(token, tokenSecret, profile, done) {
    process.nextTick(function () {
      return done(null, profile);
    });
  }
));




const app = express.createServer();

app.configure(function() {
  app.set('views', __dirname + '/views');
  app.set('view engine', 'ejs');
  app.use(express.logger());
  app.use(express.cookieParser());
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(express.session({ secret: 'react7' }));
  app.use(passport.initialize());
  app.use(passport.session());
  app.use(app.router);
  app.use(express.static(__dirname + '/public'));
});


app.get('/', function(req, res){
  res.render('index', { user: req.user });
});

app.get('/account', ensureAuthenticated, function(req, res){
  res.render('account', { user: req.user });
});

app.get('/login', function(req, res){
  res.render('login', { user: req.user });
});

app.get('/auth/bitbucket',
  passport.authenticate('bitbucket'),
  function(req, res){
  });


app.get('/auth/bitbucket/callback', 
  passport.authenticate('bitbucket', { failureRedirect: '/login' }),
  function(req, res) {
    res.redirect('/');
  });

app.get('/logout', function(req, res){
  req.logout();
  res.redirect('/');
});

app.listen(3000);

function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) { return next(); }
  res.redirect('/login')
}