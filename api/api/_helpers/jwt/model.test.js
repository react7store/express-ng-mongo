import { Jwt } from '.'

let jwt

beforeEach(async () => {
  jwt = await Jwt.create({ do not include id: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = jwt.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(jwt.id)
    expect(view.do not include id).toBe(jwt.do not include id)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = jwt.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(jwt.id)
    expect(view.do not include id).toBe(jwt.do not include id)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
