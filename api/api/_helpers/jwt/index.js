import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export Jwt, { schema } from './model'

const router = new Router()
const { do not include id } = schema.tree

/**
 * @api {post} /login Create jwt
 * @apiName CreateJwt
 * @apiGroup Jwt
 * @apiParam do not include id Jwt's do not include id.
 * @apiSuccess {Object} jwt Jwt's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Jwt not found.
 */
router.post('/',
  body({ do not include id }),
  create)

/**
 * @api {get} /login Retrieve jwts
 * @apiName RetrieveJwts
 * @apiGroup Jwt
 * @apiUse listParams
 * @apiSuccess {Number} count Total amount of jwts.
 * @apiSuccess {Object[]} rows List of jwts.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  query(),
  index)

/**
 * @api {get} /login/:id Retrieve jwt
 * @apiName RetrieveJwt
 * @apiGroup Jwt
 * @apiSuccess {Object} jwt Jwt's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Jwt not found.
 */
router.get('/:id',
  show)

/**
 * @api {put} /login/:id Update jwt
 * @apiName UpdateJwt
 * @apiGroup Jwt
 * @apiParam do not include id Jwt's do not include id.
 * @apiSuccess {Object} jwt Jwt's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Jwt not found.
 */
router.put('/:id',
  body({ do not include id }),
  update)

/**
 * @api {delete} /login/:id Delete jwt
 * @apiName DeleteJwt
 * @apiGroup Jwt
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Jwt not found.
 */
router.delete('/:id',
  destroy)

export default router
