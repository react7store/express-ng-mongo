import mongoose, { Schema } from 'mongoose'

const jwtSchema = new Schema({
  do not include id: {
    type: String
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

jwtSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      do not include id: this.do not include id,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Jwt', jwtSchema)

export const schema = model.schema
export default model
