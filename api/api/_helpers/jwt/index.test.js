import request from 'supertest'
import { apiRoot } from '../../config'
import express from '../../services/express'
import routes, { Jwt } from '.'

const app = () => express(apiRoot, routes)

let jwt

beforeEach(async () => {
  jwt = await Jwt.create({})
})

test('POST /login 201', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ do not include id: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.do not include id).toEqual('test')
})

test('GET /login 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(200)
  expect(Array.isArray(body.rows)).toBe(true)
  expect(Number.isNaN(body.count)).toBe(false)
})

test('GET /login/:id 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${jwt.id}`)
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(jwt.id)
})

test('GET /login/:id 404', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})

test('PUT /login/:id 200', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${jwt.id}`)
    .send({ do not include id: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(jwt.id)
  expect(body.do not include id).toEqual('test')
})

test('PUT /login/:id 404', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ do not include id: 'test' })
  expect(status).toBe(404)
})

test('DELETE /login/:id 204', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${jwt.id}`)
  expect(status).toBe(204)
})

test('DELETE /login/:id 404', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})
