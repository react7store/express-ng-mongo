import { success, notFound } from '../../services/response/'
import { Jwt } from '.'

export const create = ({ bodymen: { body } }, res, next) =>
  Jwt.create(body)
    .then((jwt) => jwt.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Jwt.count(query)
    .then(count => Jwt.find(query, select, cursor)
      .then((jwts) => ({
        count,
        rows: jwts.map((jwt) => jwt.view())
      }))
    )
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  Jwt.findById(params.id)
    .then(notFound(res))
    .then((jwt) => jwt ? jwt.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  Jwt.findById(params.id)
    .then(notFound(res))
    .then((jwt) => jwt ? Object.assign(jwt, body).save() : null)
    .then((jwt) => jwt ? jwt.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  Jwt.findById(params.id)
    .then(notFound(res))
    .then((jwt) => jwt ? jwt.remove() : null)
    .then(success(res, 204))
    .catch(next)
