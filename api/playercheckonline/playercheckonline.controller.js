const express = require('express');
const router = express.Router();
const playercheckonline = require('./playercheckonline');

router.get('/playercheckonline', (req, res) => {
    res.send(playercheckonline)
});

module.exports = router;