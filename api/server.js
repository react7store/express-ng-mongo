require('rootpath')();
const express = require('express');
const app = express();
const cors = require('cors');
const path = require('path');
const bodyParser = require('body-parser');
const jwt = require('_helpers/jwt');
const errorHandler = require('_helpers/error-handler');

app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());
app.use(cors());

// use JWT auth to secure the api
// app.use(jwt());

// CONTACTS API ROUTES BELOW

// Generic error handler used by all endpoints.
function handleError(res, reason, message, code) {
    console.log("ERROR: " + reason);
    res.status(code || 500).json({"error": message});
  }
  
  /*  "/api/contacts"
   *    GET: finds all contacts
   *    POST: creates a new contact
   */
  
  app.get("/api/contacts", function(req, res) {
  });
  
  app.post("/api/contacts", function(req, res) {
  });
  
  /*  "/api/contacts/:id"
   *    GET: find contact by id
   *    PUT: update contact by id
   *    DELETE: deletes contact by id
   */
  
  app.get("/api/contacts/:id", function(req, res) {
  });
  
  app.put("/api/contacts/:id", function(req, res) {
  });
  
  app.delete("/api/contacts/:id", function(req, res) {
  });

// api routes
app.use('/users', require('./users/users.controller'));
app.use('/games', require('./games/games.controller'));
app.use('/playerlist', require('./playerlist/playerlist.controller'));
app.use('/playerbalance', require('./playerbalance/playerbalance.controller'));
app.use('/playercreate', require('./users/users.controller'));
app.use('/playercheckonline', require('./playercheckonline/playercheckonline.controller'));

// global error handler
app.use(errorHandler);

// start server
const port = process.env.NODE_ENV === 'production' ? 80 : 4000;
const server = app.listen(port, function () {
    console.log('Server listening on port ' + port);
});