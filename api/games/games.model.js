const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    gameName: {
        type: String,
        required: true,
    },
    subGameName: {
        type: String,
        required: true
    },
    gameCode: {
        type: String,
        required: true,
        unique: true,
    },
    extraGameCode: {
        type: String,
        required: true
    }
});

schema.set('toJSON', {
    virtuals: true
});

module.exports = mongoose.model('Game', schema);