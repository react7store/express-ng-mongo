$(document).ready(function () {

    // SHOW CLASSES
    const searchToggle = function () {
        if ($(".search-modal").hasClass("search-modal-active")) {
            $(".navbar-search").removeClass("navbar-btn-active");
            $(".search-modal").fadeOut(300);
            $(".search-modal").removeClass("search-modal-active");

        } else {
            $(".search-modal").fadeIn(300);
            $(".search-modal").addClass("search-modal-active");
            $(".navbar-search").addClass("navbar-btn-active");
            $('#searchbar').focus();
        }
    }

    const searchRemove = function () {
        $(".navbar-search").removeClass("navbar-btn-active");
        $(".search-modal").fadeOut(300);
        $(".search-modal").removeClass("search-modal-active");
    }

    const categoryToggle = function () {
        if ($("#category-filter-dropdown").hasClass("search-filter-dropdown-active")) {
            $("#category-filter").removeClass("search-filter-active");
            $("#category-filter-dropdown").fadeOut(300);
            $("#category-filter-dropdown").removeClass("search-filter-dropdown-active");

        } else {
            $("#category-filter-dropdown").fadeIn(300);
            $("#category-filter-dropdown").addClass("search-filter-dropdown-active");
            $("#category-filter").addClass("search-filter-active");
        }
    }

    const categoryRemove = function () {
        $("#category-filter").removeClass("search-filter-active");
        $("#category-filter-dropdown").fadeOut(300);
        $("#category-filter-dropdown").removeClass("search-filter-dropdown-active");
    }

    const loginToggle = function () {
        if ($("#login-modal").hasClass("modal-active")) {
            $("#login-modal").fadeOut(200);
            $("#login-modal").removeClass("modal-active");
        } else {
            $("#login-modal").fadeIn(200);
            $("#login-modal").addClass("modal-active");
        }
    }

    const loginRemove = function () {
        $("#login-modal").fadeOut(200);
        $("#login-modal").removeClass("modal-active");
    }

    const signupToggle = function () {
        if ($("#signup-modal").hasClass("modal-active")) {
            $("#signup-modal").fadeOut(200);
            $("#signup-modal").removeClass("modal-active");
        } else {
            $("#signup-modal").fadeIn(200);
            $("#signup-modal").addClass("modal-active");
        }
    }

    const signupRemove = function () {
        $("#signup-modal").fadeOut(200);
        $("#signup-modal").removeClass("modal-active");
    }

    const comingSoonToggle = function () {
        if ($("#coming-soon-modal").hasClass("modal-active")) {
            $("#coming-soon-modal").fadeOut(200);
            $("#coming-soon-modal").removeClass("modal-active");
        } else {
            $("#coming-soon-modal").fadeIn(200);
            $("#coming-soon-modal").addClass("modal-active");
        }
    }

    const comingSoonRemove = function () {
        $("#coming-soon-modal").fadeOut(200);
        $("#coming-soon-modal").removeClass("modal-active");
    }

    const modalRemove = function () {
        $(".modal").fadeOut(200);
        $(".modal").removeClass("modal-active");
    }

    const sidebarToggle = function () {
        if ($(".navbar-sidebar").hasClass("navbar-sidebar-active")) {
            $(".navbar-sidebar").removeClass("navbar-sidebar-active");
            $(".site-wrapper").removeClass("site-wrapper-pushed");
            $(".tint").removeClass("tint-active");
            // $("footer").removeClass("footer-pushed");
        } else {
            $(".navbar-sidebar").addClass("navbar-sidebar-active");
            $(".site-wrapper").addClass("site-wrapper-pushed");
            $(".tint").addClass("tint-active");
            // $("footer").addClass("footer-pushed");
        }
    }

    const sidebarRemove = function () {
        $(".navbar-sidebar").removeClass("navbar-sidebar-active");
        $(".site-wrapper").removeClass("site-wrapper-pushed");
        $(".tint").removeClass("tint-active");
        $("footer").removeClass("footer-pushed");
    }

    const paymentToggle = function () {
        if ($("#payment-modal").hasClass("modal-active")) {
            $("#payment-modal").fadeOut(200);
            $("#payment-modal").removeClass("modal-active");
        } else {
            $("#payment-modal").fadeIn(200);
            $("#payment-modal").addClass("modal-active");
        }
    }

    const paymentRemove = function () {
        $("#payment-modal").fadeOut(200);
        $("#payment-modal").removeClass("modal-active");
    }

    const navbarProfileBtnToggle = function () {
        if ($(".navbar-profile-dropdown").hasClass(".navbar-profile-dropdown-active")) {
            $(".navbar-profile-dropdown").fadeOut(200);
            $(".navbar-profile-dropdown").removeClass(".navbar-profile-dropdown-active");
        } else {
            $(".navbar-profile-dropdown").fadeIn(200);
            $(".navbar-profile-dropdown").addClass(".navbar-profile-dropdown-active");
        }
    }

    $(".navbar-search").click(function () {
        searchToggle();
    });

    $("#category-filter").click(function () {
        categoryToggle();
    });

    $(".modal-close").click(function () {
        modalRemove();
    });

    $(".navbar-login-btn, .navbar-btm-login").click(function () {
        if ($("#signup-modal").hasClass("modal-active")) {
            signupRemove();
            loginToggle();
        }
        else {
            loginToggle();
        }
    });

    $(".navbar-signup-btn, .navbar-btm-signup").click(function () {
        if ($("#login-modal").hasClass("modal-active")) {
            loginRemove();
            signupToggle();
        }
        else {
            signupToggle();
        }
    });

    $(".navbar-burger").click(function () {
        sidebarToggle();
    });

    $(".sidebar-deposit-button").click(function () {
        paymentToggle();
    });

    $(".settings-balance-panel-deposit-button").click(function () {
        paymentToggle();
    });

    $(".sidebar-close").click(function () {
        sidebarRemove();
    });

    $(".sidebar-item").click(function () {
        sidebarRemove();
    });

    //COMING SOON
    $("#navbar-slots-link, #navbar-live-casino-link, #navbar-sports-link, #navbar-new-arrivals-link").click(function () {
        if ($("#coming-soon-modal").hasClass("modal-active")) {

        }
        else {
            comingSoonToggle();
        }
    });

    // ELEMENT FOCUS
    $(window).click(function () {
        categoryRemove();
        searchRemove();
        loginRemove();
        signupRemove();
        paymentRemove();
        sidebarRemove();
        comingSoonRemove();
    });

    $('#category-filter, #category-filter-dropdown, .navbar-search, .search-panel, .navbar-login-btn, .navbar-btm-login, .login-panel-wrapper, .navbar-signup-btn, .navbar-btm-signup, .signup-panel, .navbar-burger, .navbar-sidebar, .payment-panel, .sidebar-deposit-button, .settings-balance-panel-button, #navbar-slots-link, #navbar-live-casino-link, #navbar-sports-link, #navbar-new-arrivals-link, .coming-soon-panel').click(function (event) {
        event.stopPropagation();
    });

    // FLICKITY
    $('.main-carousel').flickity({
        // options
        cellAlign: 'center',
        contain: true,
        wrapAround: true,
    });

    // SLIDER
    $(".slider-next-btn").click(function () {

    });

    //NAVBAR BG
    $(window).on("scroll", function () {
        if ($("body").scrollTop() > 10) {
            $('.navbar-main').addClass('navbar-bg');
        } else if ($("body").scrollTop() < 10) {
            $('.navbar-main').removeClass('navbar-bg');
            $('.navbar-main').addClass('navbar-bg-initial');
        }
    });

    // $(window).on("scroll", function () {
    //     if ($("body").scrollTop() > 10) {
    //         $('.navbar-sub').addClass('navbar-bg-2');
    //     } else if ($("body").scrollTop() < 10) {
    //         $('.navbar-sub').removeClass('navbar-bg-2');
    //         $('.navbar-sub').addClass('navbar-bg');
    //     }
    // });

    //PAYMENT MODAL

    //PROFILE BTN
    // $(".navbar-profile-btn").mouseenter(function () {
    //     // $('.navbar-profile-dropdown').css({
    //     //     'display': 'block'
    //     // });
    //     $('.navbar-profile-dropdown').fadeIn(300);

    //     $('.navbar-profile-btn').css({
    //         'background-color': '#60ab2e',
    //         'border-color': '#60ab2e',
    //     });
    // });
    // $(".navbar-profile").mouseleave(function () {
    //     // $('.navbar-profile-dropdown').css({
    //     //     'display': 'none'
    //     // });
    //     $('.navbar-profile-dropdown').fadeOut(300);

    //     $('.navbar-profile-btn').css({
    //         'background-color': 'transparent',
    //         'border-color': 'rgba(128, 128, 128, 0.39)',
    //     });
    // });

    $(".navbar-profile").click(function () {
        navbarProfileBtnToggle();
    });





});