export class Games {
    id: number;
    gameName: string;
    subGameName: string;
    gameCode: string;
    extraGameCode: string;
}