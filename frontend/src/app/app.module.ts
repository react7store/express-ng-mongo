import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { Routing } from './app.routing';
import { Global } from './_shared';


import { AlertComponent } from './_components/alert/alert.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { GameComponent } from './game/game.component';
import { JwtInterceptor, ErrorInterceptor } from './_helpers';
import { SidebarComponent } from './_components/sidebar/sidebar.component';
import { SearchModalComponent } from './_components/search-modal/search-modal.component';
import { LoginModalComponent } from './_components/login-modal/login-modal.component';
import { SignupModalComponent } from './_components/signup-modal/signup-modal.component';
import { PaymentModalComponent } from './_components/payment-modal/payment-modal.component';
import { ComingSoonModalComponent } from './_components/coming-soon-modal/coming-soon-modal.component';
import { NavbarComponent } from './_components/navbar/navbar.component';
import { FooterComponent } from './_components/footer/footer.component';
import { GamesListComponent } from './_components/games-list/games-list.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { BalanceComponent } from './balance/balance.component';
import { WithdrawComponent } from './withdraw/withdraw.component';
import { DepositComponent } from './deposit/deposit.component';
import { FaqComponent } from './faq/faq.component';
import { HistoryComponent } from './history/history.component';
import { SettingsSidebarComponent } from './_components/settings-sidebar/settings-sidebar.component';
import { TransferComponent } from './transfer/transfer.component';
import { SettingsComponent } from './settings/settings.component';
import { TermsComponent } from './terms/terms.component';

@NgModule({
  declarations: [
    AppComponent,
    AlertComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    GameComponent,
    SidebarComponent,
    SearchModalComponent,
    LoginModalComponent,
    SignupModalComponent,
    PaymentModalComponent,
    ComingSoonModalComponent,
    NavbarComponent,
    FooterComponent,
    GamesListComponent,
    AboutUsComponent,
    BalanceComponent,
    WithdrawComponent,
    DepositComponent,
    FaqComponent,
    HistoryComponent,
    SettingsSidebarComponent,
    TransferComponent,
    SettingsComponent,
    TermsComponent
  ],
  imports: [
    BrowserModule,
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    Routing,
    FormsModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    Global,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
