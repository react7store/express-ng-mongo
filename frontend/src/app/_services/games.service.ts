import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Games } from '../_models';
import { Global } from '../_shared';


@Injectable({ providedIn: 'root' })
export class GamesService {
    constructor(private http: HttpClient, private Global: Global) { 
        
    }

    getAll() {
        return this.http.get<Games[]>(`${this.Global.APIUrl}/games`);
    }

    getById(id: number) {
        return this.http.get(`${this.Global.APIUrl}/games/${id}`);
    }

}