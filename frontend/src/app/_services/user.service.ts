import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from '../_models';
import { Global } from '../_shared';


@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(private http: HttpClient, private Global: Global) { 
        
    }

    getAll() {
        return this.http.get<User[]>(`${this.Global.APIUrl}/users`);
    }

    getById(id: number) {
        return this.http.get(`${this.Global.APIUrl}/users/${id}`);
    }

    register(user: User) {
        return this.http.post(`${this.Global.APIUrl}/users/register`, user);
    }

    update(user: User) {
        return this.http.put(`${this.Global.APIUrl}/users/${user.id}`, user);
    }

    delete(id: number) {
        return this.http.delete(`${this.Global.APIUrl}/users/${id}`);
    }
}