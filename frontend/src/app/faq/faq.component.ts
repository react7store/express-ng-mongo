import { Component, OnInit } from '@angular/core';

// JQUERY
declare var $: any;

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.css']
})
export class FaqComponent implements OnInit {

  constructor() { }

  ngOnInit() {
        // FAQ
        $('.faqlist li .question').click(function () {
          $(this).find('.plus-minus-toggle').toggleClass('collapsed');
          $(this).parent().toggleClass('active');
        });
  }

}
