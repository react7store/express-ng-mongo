import { Component, OnInit, OnDestroy } from '@angular/core';
// import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';

import { Games } from '../../_models';
import { GamesService } from '../../_services';

@Component({
  selector: 'games-list',
  templateUrl: './games-list.component.html',
  styleUrls: ['./games-list.component.css']
})
export class GamesListComponent implements OnInit, OnDestroy {
  // subscription: Subscription;
  games: Games[] = [];

  constructor(private gamesService: GamesService) {

  }

  ngOnInit() {
    this.loadAllGames();
  }

  ngOnDestroy() {
  }

  private loadAllGames() {
    this.gamesService.getAll().pipe(first()).subscribe(games => {
      this.games = games;
    });
  }

}
