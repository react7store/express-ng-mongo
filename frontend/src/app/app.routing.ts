import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { GameComponent } from './game/game.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { BalanceComponent } from './balance/balance.component';
import { DepositComponent } from './deposit/deposit.component';
import { FaqComponent } from './faq/faq.component';
import { HistoryComponent } from './history/history.component';
import { TransferComponent } from './transfer/transfer.component';
import { AuthGuard } from './_guards';
import { WithdrawComponent } from './withdraw/withdraw.component';
import { SettingsComponent } from './settings/settings.component';
import { TermsComponent } from './terms/terms.component';

const appRoutes: Routes = [
    { path: '', component: HomeComponent, pathMatch: 'full'},
    { path: 'game/:gamecode', component: GameComponent},
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'faq', component: FaqComponent },
    { path: 'terms', component: TermsComponent },
    { path: 'about-us', component: AboutUsComponent },
    { path: 'deposit', component: DepositComponent,canActivate: [AuthGuard] },
    { path: 'settings', component: SettingsComponent,canActivate: [AuthGuard] },
    { path: 'transfer', component: TransferComponent,canActivate: [AuthGuard] },
    { path: 'withdraw', component: WithdrawComponent,canActivate: [AuthGuard] },
    { path: 'history', component: HistoryComponent,canActivate: [AuthGuard] },
    { path: 'balance', component: BalanceComponent,canActivate: [AuthGuard] },

    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const Routing = RouterModule.forRoot(appRoutes);