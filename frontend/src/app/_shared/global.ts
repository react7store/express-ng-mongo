import { Injectable } from '@angular/core';

@Injectable()
export class Global {
    readonly APIUrl: string = 'http://localhost:4000';
}