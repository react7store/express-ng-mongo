import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';

import { Games } from '../_models';
import { GamesService } from '../_services';
import { ActivatedRoute } from "@angular/router";

import { DomSanitizer } from '@angular/platform-browser';

@Component({ templateUrl: 'game.component.html', styleUrls: ['./game.component.css'] })
export class GameComponent implements OnInit, OnDestroy {
  games: Games[] = [];
  gamecode = "";
  gameurl = "";

  constructor(
    private gamesService: GamesService,
    private route: ActivatedRoute,
    private _sanitizationService: DomSanitizer
  ) {

  }
  ngOnInit() {
    this.loadAllGames();
    window.scrollTo(0, 0);
    this.route.paramMap.subscribe(code => {
      this.gamecode = code.get("gamecode");
      this.gameurl = (`http://cache.download.banner.fourblessings88.com/casinoclient.html?language=en&game=${this.gamecode}&mode=offline`);
    });

  }

  ngOnDestroy() {

  }

  private loadAllGames() {
    this.gamesService.getAll().pipe(first()).subscribe(games => {
      this.games = games;
    });
  }

}

// 1. get game code (success)
// 2. identify game by game code (??) - search db w/ code
// 3. extract game demo url from db entry (??)
// 4. display (ez)