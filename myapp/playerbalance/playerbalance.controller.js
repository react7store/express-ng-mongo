const express = require('express');
const router = express.Router();
const playerbalance = require('./playerbalance');

router.get('/playerbalance', (req, res) => {
    res.send(playerbalance)
});

module.exports = router;