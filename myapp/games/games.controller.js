const express = require('express');
const router = express.Router();
const gameService = require('./games.service');

// routes
router.get('/', getAll);


module.exports = router;

function getAll(req, res, next) {
    gameService.getAll()
        .then(games => res.json(games))
        .catch(err => next(err));
}

function getById(req, res, next) {
    gameService.getById(req.params.id)
        .then(game => game ? res.json(game) : res.sendStatus(404))
        .catch(err => next(err));
}
