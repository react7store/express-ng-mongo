const config = require('config.json');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const db = require('_helpers/db');
const Game = db.Game;

module.exports = {
    getAll,
};

async function getAll() {
    return await Game.find().select('-hash');
}

async function getById(id) {
    return await Game.findById(id).select('-hash');
}

