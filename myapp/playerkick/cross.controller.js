const express = require('express');
const router = express.Router();
const playerKick = require('./playerKick');

router.get('/playerKick', (req, res) => {
    res.send(playerKick)
});

module.exports = router;